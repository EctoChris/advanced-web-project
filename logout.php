<?php
session_start();
//get the page the user comes from
$origin = $_SERVER['HTTP_REFERER'];
//unset session variables
unset($_SESSION["username"]);
unset($_SESSION["email"]);
unset($_SESSION["account_id"]);
unset($_SESSION["shoppingCartObject"]); //added this line recently

//redirect the user to $origin
header("location: $origin");

?>