console.clear();

var root = document.documentElement;
var key = 32; // SPACE
var pressed = false;
var pressedTime = 0;
var pressedTarget = 4000; // miliseconds

document.addEventListener('keydown', function(event) {
    if (event.keyCode === key) {
        event.preventDefault();

        pressed = true;
    }
});
document.addEventListener('keyup', function(event) {
    if (event.keyCode === key) {
        event.preventDefault();

        pressed = false;
    }
});

document.addEventListener('touchstart', function(event) {
    event.preventDefault();

    pressed = true;
});
document.addEventListener('touchend', function(event) {
    event.preventDefault();

    pressed = false;
});

var pressTimer = setInterval(function() {
    if (pressed) {
        pressedTime += 100;
    }
    else {
        pressedTime = 0;
    }

    root.style.setProperty('--progress', pressedTime / pressedTarget);

    if (pressedTime >= pressedTarget) {
        clearInterval(pressTimer);
        document.body.classList.add('js-pressed');
    }
}, 100);
