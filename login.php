<?php   include('autoloader.php');  

//handle POST request
if( $_SERVER["REQUEST_METHOD"] == "POST" ){
  $credentials = $_POST["credentials"];
  $password = $_POST["password"];
  //create instance of account class
  $account = new Account();
  $login = $account -> authenticate( $credentials, $password );
  if( $login == true ){
    //all good
    $destination = "index.php";
    if($_SESSION['origin'])
    {
    	$lastPage = $_SESSION['origin'];
    	header("location: $lastPage");
    }
    else {
    header("location: $destination");
    }
  }
  else{
    //get errors
    $errors = $account -> errors;
    echo count ( $account -> errors );
  }
}



?>

<!doctype html>
<html>
     <?php  include('includes/head.php'); ?>
    <body>
          <?php include('includes/navbar.php'); ?>
          
        
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		  <link href="../css/styles.css" rel="stylesheet" type="text/css">
		  <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
		  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" rel="stylesheet">
		
		 
			<div class="container" >
			    <div class="row" style="min-height: 58vh;">
					<div class="colorPanel col-md-4 col-md-4  justify-content-center align-items-center mt-5 container">
				
				
				<!-- Johanne's php -->	
		     	<?php
			          if( count($account -> errors) > 0 )
			          {
			            $error_string = implode(' ', $account -> errors );
			             $alert = "<div class=\"alert alert-warning alert-dismissible fade show\" role=\"alert\">
			                      $error_string
			                      <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
			                        <span aria-hidden=\"true\">&times;</span>
			                      </button>
			                    </div>";
			            echo $alert;
			          }
                 ?>  <!-- End of Johannes's php-->
                 
						
						<!-- Diego's beautiful login form -->
			    		<div class="panel panel-default">
						  	<div class=" panel-heading">
						    	<h3 class=" panel-title">Please Login</h3>
						 	</div>
						  	<div class=" panel-body">			    	
							  	<form id="login-form" method="post" action="login.php" accept-charset="UTF-8" role="form" novalidate>
					                    <fieldset>
								    	  	<div class="form-group">
								    		    <input id="credentials" class="form-control " placeholder="E-mail" name="credentials" type="text" required>
								    		    <div class="invalid-feedback">Please type a valid username or email</div>
	
								    		</div>
								    		<div class="form-group">
								    			<input id="password" class="form-control" placeholder="Password" name="password" type="password" value="" required>
								    			<div class="invalid-feedback">Please type a valid password</div>
	
								    		</div>
								    		<div class="checkbox">
								    	    	<label>
								    	    		<input name="remember" type="checkbox" value="Remember Me"> Remember Me
								    	    	</label>
								    	    </div>
								    	   <button type="submit" id="loginbutton" name="login" class="btn btn-lg btn-success btn-block">Log in</button>

								    	<!--<input class="btn btn-lg btn-success btn-block" type="submit" value="Login"> -->
								    	</fieldset>
							     </form>
						   </div>
						</div> <!-- End of Deigo's Beautiful Login panel -->
						<a href="./register.php">Register now</a>
					</div>
				</div>
	     	</div> 
		
		<!-- Johannes's weird template -->
		<template id="alert-template">
		  <div class="alert alert-dismissible fade show" role="alert">
		    <span class="alert-message"></span>
		    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		      <span aria-hidden="true">&times;</span>
		    </button>
		  </div>
		</template>   		<!-- End of Johannes's weird template -->

		<!--FOOTER using bootstrap grid / container -->
		<br>
		<br>
		<br>
			<footer class="footer inline bg-secondary">
				<div class="container">
					<div class="fonthrybrids text-center">
						<p style="font-family: 'Lobster', cursive;color:#ffffff"> Trichohybrids</p>
				    </div>

					<div class="bottom-footer mt-10">
						<div class="row">
                         <div class="col-md-1 mr-10 mb-15"><img src="images/cactusfooter.png"></img></div>
                         <div class="col-md-3 ml-7 mt-4"> <h6><small>&copy;Copyright TrichoHybrids Inc</small></h6> </div>
						  <div class="col-md-5 ml-20 mt-20">
						  	<ul class="list-unstyled footer-nav d-flex justify-content-left">
							<li><a href="index.php">Home</a></li>
							<li><a href="allproductspage.php">All Products</a></li> 	
							<li><a href="contactus.php"> Contact Us</a></li> 	
							<li><a href="login.php">Login</a></li> 
						    <li><a href="register.php">Register</a></li> 
						
						 </ul>
						  </div>
						  <div class="col-md-3 d-flex justify-content-end">
						  <a href="https://www.instagram.com/trichohybrids/" target="_blank"><i class="fa fa-instagram" style="font-size:40px;color:#ffffff"></i></a>
						  </div>
						</div>
		     		</div>
		     	
		     	</div>	
			</footer>
			
		<!-- END FOOTER -->
			
	    <script src="/js/login.js"></script> 
	</body>
</html>	
	


