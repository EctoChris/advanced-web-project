<?php
include('autoloader.php');
session_start();

//Create an instance of Products class
$products_obj = new Products();


if (isset($_GET['categorySelection']))
{
  $total_in_page = $products_obj -> products_total;
  $total_items = $products_obj -> products_total;
}
else if (isset($_GET['searchTerm']))
{
    //$products = $products_obj -> getProducts(false);
    $total_items = $products_obj -> products_total;
    $total_in_page = $products_obj -> products_total;
}
else {
  $total_in_page = $products_obj -> current_page_total;
  $products = $products_obj -> getProducts(true);
  $total_items = $products_obj -> products_total;
}

// Notes from johannes -> on category link, append the link with ? and variable to post it. Get category will just get the variable


//Create instance of Categories class
$categories_obj = new Categories();
$categories = $categories_obj -> getCategories(true);

// print_r($products);
// echo" break ";
// print_r($total_items);
//CURRENT PAGE NUMBER
$page_var = $_GET["page"];
//if page number is valid, use it
if( filter_var( $_GET["page"], FILTER_VALIDATE_INT) ){
  $current_page_number = $_GET["page"];
}
//set page number to 1 if invalid
else{
  $current_page_number = 1;
}

//CURRENT CATEGORY ID
if (isset($_GET['categorySelection']))
{
  $category_var = $_GET['categorySelection'];
  $products = $products_obj ->getProducts(false);
  $total_items = $products_obj -> products_total;
}

else if (isset($_GET['searchTerm']))
{
    $products = $products_obj -> getProducts(false);
    $total_items = $products_obj -> products_total;
    $total_in_page = $products_obj -> products_total;
}

if( filter_var($category_var, FILTER_VALIDATE_INT ) ){
  $current_category = $category_var;
}
else{
  $current_category = 0;
}

$page_title = "Home page";
?>


<!doctype html>
<html>
  <?php include ('includes/head.php'); ?>
  <body>
    <?php include('includes/navbar.php'); ?>
    <div class="container">

        <div class='display-1 text-center'>
        <?php 
        if (isset($_GET['categorySelection']))
        {
          switch($_GET['categorySelection'])
          {
            case 1:
              echo "Trichocereus Pachanoi Products";
             break;
            case 2:
              echo "Trichocereus Bridgesi Products";
              break;
            case 3: 
             echo "Trichocereus Peruvianus Products";
             break;
          }
        } 
        else if (isset($_GET['searchTerm']))
        {
           $searchWord = $_GET['searchTerm'];
           echo "Search results for '" . $searchWord . "'";
        }
        else
        {
          echo "All Products";
        }
          ?>
        </div>
  
      <!-- Pagination Buttons & Categories -->
      <div class="row d-flex justify-content-between align-items-center">
          <!-- Pagination Buttons -->
          <div>
            <div class="product-nav my-md-4 my-sm-2 text-center">
              <div>
                <?php include('includes/pagination.php');  ?>
              </div>
            </div>
          </div>
          
          <!-- Categories Dropdown -->
          <form method="get" action="allproductspage.php">
              <select name="categorySelection">
                  <option value="1">Trichocereus Pachanoi</option>
                  <option value="2">Trichocereus Bridgesi</option>
                  <option value="3">Trichocereus Peruvianus</option>
              </select>
              
              <input type="submit" value="Search By Category"/>
          </form>
      </div> <!-- End of Categories & Pagination -->
        
        <!-- Current Page & Total Pages Display -->
       <div class="row d-flex justify-content-between">
          <div class="product-nav-text">
                <?php
                if (!isset($_GET['categorySelection']) && (!isset($_GET['searchTerm'])))
                {
                    $total_pages = Pagination::$total_pages;
                    echo "Page $current_page_number of $total_pages"; 
                }
                ?>
          </div>
              <?php 
              if (!isset($_GET['categorySelection']) && (!isset($_GET['searchTerm'])))
              {
                echo "<div class='text-right product-nav-text'>";
               echo "<span class='d-none d-sm-inline'>Total of </span>" .  $total_items . "  <span class='d-sm-inline'>products</span>";
               echo "</div>";
              }
          ?>
       </div>
          
          <!--Displaying of all products-->
          <?php
          if( count($products) > 0 && $products){
            $col_counter = 0;
            $product_counter = 0;
            foreach( $products as $product ){
              $col_counter++;
              $product_counter++;
              if( $col_counter == 1 )
              {
                echo "<div class='container-fluid'>";
                echo "<div class=\"row align-middle row justify-content-md-center text-center\">";
              }
              
              //print out columns
              $id = $product["id"];
              $name = $product["name"];
              $price = $product["price"];
              $image = $product["imagename"];

              echo "<div class=\"col-4 product-column\">";
              echo "<a href=\"detail.php?product_id=$id\">";
              echo "<h4 class=\"product-name\">$name</h4>";
              echo "<img class=\"product-thumbnail img-fluid\" src=\"images/products/$image\">";
              echo "<h5 class=\"price product-price\">$price</h5>";
              echo "</a>";
              echo "</div>";
              if($col_counter == 3 || $product_counter ==  $total_items)
              {
                echo "</div>";
                echo "<hr>";
                echo "</div>";
                $col_counter = 0;
              }
            }
          }
            include('includes/pagination.php');
          ?>
        </div>
      </div>
      <?php include('includes/spacebar.php'); ?>

      <?php include('includes/footer.php'); ?>
  </body>
</html>