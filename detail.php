<?php
include("autoloader.php");
session_start();

//include if condition for logged in here
if( isset($_GET["product_id"])  || isset($_SESSION['currentProductId']) )
{
  $product_id = $_GET["product_id"];
  //if (!isset($_SESSION['currentProductId']))
  if($_SESSION['currentProductId'] != $product_id && $product_id != null)
  {
      $_SESSION['currentProductId'] = $product_id;
  }

  $product_detail = new ProductDetail( $product_id );
  $product = $product_detail -> product;
  
  $product_name = $product[0]["name"];
  $product_price = $product[0]["price"];
  $product_description = $product[0]["description"];
  
  
  if (isset($_POST["AddToCart"]))
  {
        if($_SESSION["username"])
        {
          if(isset($_POST['quantity']))
          {
              if($_SESSION['shoppingCartObject'])
              {
                $_SESSION['shoppingCartObject'] = new ShoppingCart();
                $productQuantity = $_POST['quantity'];
                $_SESSION['shoppingCartObject'] -> addToShoppingCart($_SESSION['currentProductId'], $productQuantity, $_SESSION["id"]);
                //header ("location:detailsuccess.php");
                $_SESSION['successAddingProducts'] = true;
              }
          }
        }
        else {
              $_SESSION['origin'] = $_SERVER['REQUEST_URI'];
              header("location:login.php");
        }
   }
}
else{
  echo "You will be redirected to the home page after 5 seconds";
  header( "location:index.php" );
}
$page_title = $product_name;
?>
<!doctype html>
<html>
  <?php include ('includes/head.php'); ?>
  <body>
    <?php include('includes/navbar.php'); 
        if($_SESSION['successAddingProducts'] == true)
         {
            echo "<div class='alert alert-success' role='alert'>";
            if ($productQuantity > 1)
            {
              echo "You have successfully added " . $productQuantity . "  products to cart. Click <a href='checkout.php' class='alert-link'>here</a> to go to checkout. &nbsp; &nbsp;       <a href='allproductspage.php' class='alert-link'>        Continue Shopping </a>";
            }
            else {
              echo "You have successfully added " . $productQuantity . "  product to cart. Click <a href='checkout.php' class='alert-link'>here</a> to go to checkout. &nbsp; &nbsp;        <a href='allproductspage.php' class='alert-link'>        Continue Shopping </a>";
            }
            echo "</div>";
             $_SESSION['successAddingProducts'] = false; 
          }?>
    <div class="container content">
      <a href="allproductspage.php" class="btn btn-success"> Continue Shopping </a>

      <div class="row mt-2 justify-content-sm-center">
        <div class="col-sm-7 col-md-5 col-lg-5">
           <?php
              $image = $product[0]["image"];
              echo "<img class=\"img-fluid\" src=\"/images/products/$image\">";
           ?>
        </div> <!-- end of imageCol -->
        <div class="col-lg-7 col-md-7 col-sm-12  d-flex flex-column justify-content-between"> <!-- Start of Buy Product Info Col-->
        
              <!-- Product Name & Price-->
              <div class="product-name display-2 text-sm-center text-md-left text-lg-left">
                <?php echo $product_name; ?>
              </div>
              <h3 class="price ">
                <?php echo "Price: $" . $product_price; ?>
              </h3>
              
              <!-- Quantity Section -->
           <form action ="" method = "post">

              <div>
                  <div class="row align-items-end">
                    
                      <h3 class="col-2"> Quantity: </h3>
                      
                      <div class="col-4"></div>
                      
                       <!--use php here to query the database to get the max possible amount to buy-->
                      <input class="col-5" name="quantity" type ="number" id="spinner" min="1" value ="1" max="20"/>
                  </div>
                  <p>(Quantity of 1 = 1 ft cutting)</p>
              </div>
              
              <!-- Description Section -->
              <div>
                <h3>Description</h3>
                <p class="description">
                <?php echo $product_description; ?>
                </p>
              </div>

                  <input type="submit" class="btn btn-success btn-lg btn-block" name="AddToCart" value="Buy Now" />
                </form>
              <!--<button type="button" class="btn btn-success btn-lg btn-block"> Buy Now</button>-->
        </div> <!-- End of Buy Product Info Col-->
      </div>   <!-- End of Product Row -->
    </div>
    
    <?php include('includes/spacebar.php'); ?>
    <?php include('includes/footer.php'); ?>

  </body>
</html>