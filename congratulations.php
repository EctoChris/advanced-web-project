<?php
include("autoloader.php");
session_start();

?>

<!doctype html>
<html>
  <?php include ('includes/head.php'); ?>
  <body>
      <?php
      $_SESSION['shoppingCartObject'] = new ShoppingCart();
      $_SESSION['shoppingCartObject'] -> emptyShoppingCart($_SESSION['id']);
      include('includes/navbar.php'); ?>
      <!--<div class="container">-->
              <div class="jumbotron text-center">
                  <h1 class="display-3">Thank You <?php echo $_SESSION['username']?>!</h1>
                  <p class="lead"><strong>Please check  ' <?php echo $_SESSION['email'] ?></strong> for further instructions on how to complete your account setup.</p>
                  <hr>
                  <p>
                    Having trouble? <a href="">Contact us</a>
                  </p>
                  <p class="lead">
                    <a class="btn btn-primary btn-sm" href="index.php" role="button">Continue to homepage</a>
                  </p>
             </div>
      <!--</div>-->

 </body>

</html>