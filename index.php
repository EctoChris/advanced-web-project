<?php
  
  include('autoloader.php');
  session_start();

  //create an instance of products class
  $products_obj = new Products();
  //old one for getallproducts
  //$products = $products_obj -> getProducts('index');
  $products = $products_obj -> getBestSellers();

?>

<!doctype html>
<html>
    <?php include('includes/head.php'); ?>

    <body>
          <?php include('includes/navbar.php');?>
          
          <!-- Side Navbar and Cactus Image Section -->
            <!-- Side Navbar -->
            <?php
            if( $_SESSION["username"]) {
              $user = $_SESSION["username"];
              echo '<p class="text-center h2 bg-light">Hello '. $user .'</p>';
            } ?>
            


                   <!-- Side Navigation Bar -->
                 <div class="container-fluid backImage d-flex align-items-center">
                   
                      <div class="row d-flex align-items-center">
                        
                          <!-- Cactus Image  -->
                         <!--<img class="img-fluid offset-lg-1 offset-md-0 offset-sm-0 offset-md-0 col-12 col-sm-12" src="images/cactusexample.jpg" style="max-width: 100%">-->
                           <div class="sideNavbar p-3 col-5 bg-light row">
                             <div class="col-12">
                               <h3 class="pl-5"> Cacti Types</h3>
                                <a class="p-5 mt-2 align-self-start" href="allproductspage.php?categorySelection=1"> Trichocereus Pachanoi</a>
                                <a class="p-5 mt-2 align-self-start" href="allproductspage.php?categorySelection=2"> Trichocereus Bridgesii</a>
                                <a class="p-5 mt-2 align-self-start" href="allproductspage.php?categorySelection=3"> Trichocereus Peruvianus</a>
                                </div>
                           </div>
                      </div>

                 </div>

             
          
          <!-- Best Selling Products Display -->
          <div class="container-fluid text-center" style="padding-left: 0; padding-right: 0;">
             
              <div class="bestSellingRow bg-light">
                 <h2> Best Selling Products </h2>
               <?php
                 if (count ($products_obj -> products) > 0)
                {
                    //output the products
                    foreach( $products_obj -> products as $product)
                    {
                      $product_id = $product["id"];
                      $product_name = $product["name"];
                      $product_price = $product["price"];
                      $product_description = $product["description"];
                      $product_image = $product["imagename"];
                      
                      $col_counter++;
                      if ($col_counter == 1)
                      {
                          echo "<div class='row bestproductsrow'>";
                      }
                      echo "<div class=' col-md-4 col-sm-12 bestproductcolumn col-xs-12'>";
                      echo "<h3 class='product-name'> $product_name</h3>";
                      echo "<div class='blackborder text-center'>";
                      echo "<img class='product-thumbnail img-fluid' src=\"images/products/$product_image\">";
                      echo "</div>";
                      echo "<p>Price: $$product_price</p>";
                      //echo "<p class='price'> $product_id</p>";
                      echo "<a href=\"detail.php?product_id=$product_id\">View</a>";
                      echo "</div>";
                    }
                 }
              ?>
              </div>
        </div> <!-- End of best selling products display -->
        
      
      
       <!--    Start Carrousel      -->
       <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                  <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                  <div class="carousel-item active">
                    <img class="d-block w-100" src="images/cactusSlider1.png" alt="First slide">
                  </div>
                  <div class="carousel-item">
                    <img class="d-block w-100" src="images/cactusSlider2.png" alt="Second slide">
                  </div>
                  <div class="carousel-item">
                    <img class="d-block w-100" src="images/cactusSlider3.png" alt="Second slide">
                  </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
      </div>
        
        
         <!--Website Info -->
        <div class="welcomeBackground">
            <div class="welcomeMessage text-center mt-6">
                  <p class="text-muted"> 
                      UrbanTribes is our modest 1/4 acre block located in the Western Suburbs, Hills district of Sydney. <br>
                      Our cactus garden, established in 1991, consists of medicinal, visionary, sacred plants & cactus <br> 
                      that we have over the years and continue to amass from cuttings & grow from seed. <br> <br>
                      
                      We have many variants of interesting cacti and succulents but are particularly partial to columnar (visionary) members of the Cactaceae Family.
                  </p>
            </div>
        </div> 
        <!-- End of website Info  -->
        
        
      <?php include('includes/spacebar.php'); ?>

      <?php include('includes/footer.php'); ?>
     	   

    </body>
</html>