<nav class="navbar navbar-expand-lg navbar-light bg-light sticky-top">
    
      <!-- logo -->
      <div class="navbar-brand text-xs-center col-xs-0.5  col-sm-0.5 col-md-0.5">
           <a href="../index.php">
            <img  class="img-fluid col-xs-12 text-xs-center cactusLogo" src="images/completeCactusLogo.png" width="150" height="150" alt="">
          </a>
      </div>
      
      <!-- dropdown button -->
      <button class="navbar-toggler col-sm-1 col-xs-1 order-md-7 order-sm-7 order-xs-7" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      
      <!-- collapsable part of menu -->
      <div class="collapse navbar-collapse order-md-8 order-sm-8 order-xs-8 order-lg-2 col-lg-6" id="navbarNav">
              <ul class="nav navbar-nav navbar-right">
                  <li class="nav-item active">
                    <a class="nav-link" href="../index.php">Home <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="../allproductspage.php">All Products</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="../contact.php">Contact Us</a>
                  </li>
                  <?php
                  if(!$_SESSION["username"]) {
                      echo '<li class="nav-item">
                        <a class="nav-link" href="../login.php">Login</a>
                      </li>';
                  } else {
                    $_SESSION["shoppingCartObject"] = new ShoppingCart();
                    
                      echo '<li class="nav-item">
                        <a class="nav-link" href="../logout.php">Sign out</a>
                      </li>';
                  }
                  ?>
              </ul>
      </div>
      
       <!-- Search Bar     -->
       <div class="formContainer p-1 my-2 mr-2 my-lg-0  d-sm-flex flex-row flex-sm-nowrap order-lg-3 col-lg-3 col-sm-3">
                   <form class=" mr-2 my-lg-0  d-flex flex-row flex-sm-nowrap innerForm col-lg-12 col-sm-12" method="get" action="allproductspage.php">
                        <input class="form-control col-10 formInput" type="search" name="searchTerm" placeholder="Search" aria-label="Search">
                        <button class="btn btn-outline-* my-2 my-sm-0 mr-sm-2  d-md-inline-block searchButton col-sm-2" type="submit"><i class="fas fa-search" style="font-size:22px;color:grey;"></i></button>
                   </form>
               </div>
               
         <!-- Icons -->
                  <a href="../checkout.php" id="shoppingCartIcon" class ="shoppingCartIcon order-lg-5"> <i class="fas fa-shopping-cart "style="font-size:28px;color:grey;"> </i>    </a>        
                  <?php  
                  if($_SESSION['shoppingCartObject'] && ($_SESSION["username"]))
                  {
                        $totalProductsInCart = $_SESSION['shoppingCartObject'] -> returnTotalProducts();
                        if ($totalProductsInCart > 0)
                        {
                          echo '<a href="../checkout.php" class="badge badge-primary shoppingCartBadge order-6">' . $totalProductsInCart . ' </a>';
                        }
                  }
                   ?>
         <i class="fas fa-user-circle order-lg-4 col-lg-1 col-md-1" style="font-size:28px;color:grey;"></i>
         
</nav>

<!-- d-flex flex-row-reverse bd-highlight -->

<!-- old input: form-control mr-sm-2 flex-grow-1 formInput col-sm-10 -->