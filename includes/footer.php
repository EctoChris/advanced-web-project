	<!--FOOTER using bootstrap grid / container -->
			<footer class="footer inline bg-secondary">
				<div class="container">
					<div class="fonthrybrids text-center">
						<p style="font-family: 'Lobster', cursive;color:#ffffff"> Trichohybrids</p>
				    </div>

					<div class="bottom-footer mt-10">
						<div class="row">
               <div class="col-md-1 mr-10 mb-15"><img src="images/cactusfooter.png"></img></div>
               <div class="col-md-3 ml-7 mt-4"> <h6><small>&copy;Copyright TrichoHybrids Inc</small></h6> </div>
            
						  <div class="col-xm-5 col-xsm-12 ml-20 mt-20">
						  	<ul class="footer-nav">
  							   <li class="list-inline-item"><a href="">Home</a></li>
  						     <li class="list-inline-item"><a href="../allproductspage.php">All Products</a></li> 	
  						     <li class="list-inline-item"></li><a href="../contact.php"> Contact Us</a></li> 	
  						     <?php 
          						     if (!$_SESSION['username'])
          						     {
          						        echo  "<li class='list-inline-item'><a href='../login.php'>Login</a></li>  ";
          						     }
          						     else {
          						        echo "<li class='list-inline-item'><a href='../logout.php'>Logout</a></li>  ";
          						     }
  						      ?>
					      </ul>
						  </div>
						  <div class="col-md-3 col-sm-2  d-flex justify-content-end img-responsive">
						  <a href="https://www.instagram.com/trichohybrids/" target="_blank"><i class="fa fa-instagram" style="font-size:40px;color:#ffffff"></i></a>
						  </div>
						  
						</div>
		     		</div>
		     	
		     	</div>	
			</footer>
			
		<!-- END FOOTER -->