<nav class="navbar navbar-expand-lg navbar-light bg-light">
    
      <!-- logo -->
      <div class="navbar-brand text-xs-center col-xs-0.5  col-sm-0.5 col-md-0.5">
           <a href="#">
            <img  class="img-fluid col-xs-12 text-xs-center cactusLogo" src="images/completeCactusLogo.png" width="150" height="150" alt="">
          </a>
      </div>
      
      <!-- dropdown button -->
      <button class="navbar-toggler col-sm-1 col-xs-1 order-md-7 order-sm-7 order-xs-7" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      
      <!-- collapsable part of menu -->
      <div class="collapse navbar-collapse order-md-8 order-sm-8 order-xs-8 order-lg-2" id="navbarNav">
              <ul class="nav navbar-nav navbar-right">
                  <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">All Products</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">Contact Us</a>
                  </li>
              </ul>
      </div>
      
       <!-- Search Bar     -->
       <div class="formContainer p-1 my-2 mr-2 my-lg-0  d-sm-flex flex-row flex-sm-nowrap order-lg-3 col-lg-4 col-sm-3 col-xs-2">
                   <form class="form-inline my-2 mr-2 my-lg-0  d-sm-flex flex-row flex-sm-nowrap innerForm col-lg-12 col-sm-12" method="get" action="search.php">
                        <input class="form-control mr-sm-2 flex-grow-1 formInput col-sm-10" type="search" name="keywords" placeholder="Search" aria-label="Search">
                        <button class="btn btn-outline-* my-2 my-sm-0 mr-sm-2  d-md-inline-block searchButton col-sm-2" type="submit"><i class="fas fa-search" style="font-size:22px;color:grey;"></i></button>
                   </form>
               </div>
               
         <!-- Icons -->
         <i class="fas fa-shopping-cart order-lg-5 col-lg-0.5 col-md-0.5 col-sm-0.5 "style="font-size:28px;color:grey;"></i>            
         <i class="fas fa-user-circle order-lg-4 col-lg-0.5 col-md-0.5 col-sm-0.5 "style="font-size:28px;color:grey;"></i>
</nav>

<!-- d-flex flex-row-reverse bd-highlight -->