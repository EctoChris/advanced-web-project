<?php
include("autoloader.php");
session_start();

?>

<!doctype html>
<html>
  <?php include ('includes/head.php'); ?>
  <body>
    <?php 
      $_SESSION["shoppingCartObject"] = new ShoppingCart();

      if (isset($_POST["deleteItem"]))
      {
          if(isset($_POST["itemIdToDelete"]))
          {
              $deleteId = $_POST["itemIdToDelete"];
              $_SESSION['shoppingCartObject'] -> deleteProductFromCart($deleteId, $_SESSION['id']);
          }
      }
      
      // if(isset($_POST["buyNow"]))
      // {
      //   if($_SESSION['id'])
      //   {
      //     $_SESSION['shoppingCartObject'] -> emptyShoppingCart($_SESSION['id']);
      //   }
      // }
      
      include('includes/navbar.php'); 
    ?>
    <div class="container">
    
    <div class="row mt-5 mb-5">
        <table class="table col-lg-6 col-md-12 col-sm-12 col-xs-12">
          <thead class="thead-light">
            <tr>
              <th scope="col">Product Name</th>
              <th scope="col">Quantity</th>
              <th scope="col">Price</th>
              <th scope= "col">SubTotal</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>
              <?php
              $maxTotal = 0;
              $maxQuantity = 0;
              if($_SESSION['shoppingCartObject'])
              {
                  $_SESSION['shoppingCartObject'] -> getProductsInCart($_SESSION['id']);
                  if (count($_SESSION['shoppingCartObject'] -> productsInCart) > 0)
                  {
                      foreach($_SESSION['shoppingCartObject'] -> productsInCart as $cartProduct)
                      {
                          $productid = $cartProduct["productid"];
                          $productQuantity = $cartProduct["quantity"];
                          $_SESSION['shoppingCartObject'] -> getProductNameAndPrice($productid);
                          
                          $productInfo = $_SESSION['shoppingCartObject'] -> extraProductInfo;
                          $productCost = $productInfo['price'];
                          $imageName = $productInfo['imagename'];
                          $productName= $productInfo['name'];
                          $maxTotal += ($productCost * $productQuantity);
                          $maxQuantity += $productQuantity;
                          echo "<tr>";
                          echo "  <th scope='row'>"  . $productName . " </th>";
                          echo "  <td> " . $productQuantity . " </td>";
                          echo "  <td> $ " . $productCost . " </td>";
                          echo "  <td> $ " . $productCost * $productQuantity . " </td>";
                          echo "  <td><button type='button' class='btn btn-danger' data-toggle='modal' data-target='#exampleModalCenter' data-whatever='" . $productid . "'> Remove</button> </td>";
                          echo "</tr>";
                          
                          include('includes/modal.php');
                      }
                  }
              }
              
              ?>
             
              <!-- default row underneath to be put in loop -->
            <tr>
              <th scope="row" colspan="3">Total</th>
              <td><?php echo "$ " . $maxTotal ?></td>
            </tr>

            <tr>
           
          </tbody>
        </table>
        
        <div class="col-1"></div>
        <!-- Total Cost section and Commit to Buy -->
        <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 mb-5">
                <table class="table">
                  <thead class="thead-light">
                      <th colspan="5">Order summary</th>
                  </thead>
                  <tbody>
                    <tr>
                          <td colspan="4"> Number of Products</td>
                          <td colspan="1"> <?php echo $maxQuantity ?></td>
                    </tr>
                    <tr>
                        <td colspan="4"> Total Cost</td>
                        <td colspan="1">  <?php echo "$" . $maxTotal; ?></td>
                    </tr>
                  
                  </tbody>
                </table>
                <div class="paymentOptions row mb-3">
                    <div class="col-1"></div>
                    <div class="form-check form-check-inline col-2">
                      <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                      <img class="product-thumbnail img-fluid" src="images/paypal.png">
                    </div>
                     <div class="col-1"></div>

                    <div class="form-check form-check-inline col-2">
                      <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                      <img class="product-thumbnail img-fluid" src="images/mastercard.png">
                    </div>
                    <div class="col-1"></div>
                    <div class="form-check form-check-inline col-2">
                      <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3">
                      <img class="product-thumbnail img-fluid" src="images/amex.png">
                    </div>

                </div>  
                <?php 
                     $congratulationsURL= "";
                     if($_SESSION['shoppingCartObject'] && ($_SESSION["username"]))
                      {
                            $totalProductsInCart = $_SESSION['shoppingCartObject'] -> returnTotalProducts();
                            if ($totalProductsInCart > 0)
                            {
                              $congratulationsURL = "congratulations.php";
                            }
                      }
                  ?>
                <form action=<?php echo $congratulationsURL ?> method="post">
                   <input width="90%" type="submit" class="btn btn-warning col-12 mx-auto" name="buyNow" value="Buy Now"></input>
                </form>
        </div>
      </div>
    </div>
  </body>
  	    <script src="/js/modal.js"></script> 
           <?php include('includes/footer.php'); ?>

</html>