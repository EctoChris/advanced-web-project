<?php
class Categories extends Database{
  public function __construct(){
    //initialise database class
    parent::__construct();
  }
  public function getCategories($active_only = false)
  {
    $categories = array();
    $query = "SELECT categoryid, categoryname FROM categories";

    //no need to use statement here as query is fixed.
    $result = $this -> connection -> query( $query );
    if( $result -> num_rows > 0 ){
      while( $row = $result -> fetch_assoc() ){
        array_push( $categories, $row );
      }
      return $categories;
    }
    else{
      return null;
    }
  }
  
  public function getCategoryChildren( $parent_id, $active_only = false ){
    $categories = array();
    $query = "SELECT categoryid,categoryname FROM categories";
    if( $active_only === true ){
      $query = $query . " WHERE active=1 AND parent_id= ?";
    }
    //statement
    $statement = $this -> connection -> prepare( $query );
    $statement -> bind_param( 'i', $parent_id);
    $statement -> execute();
    $result = $statement -> get_result();

    if( $result -> num_rows > 0 ){
      while( $row = $result -> fetch_assoc() ){
        array_push( $categories, $row );
      }
      return $categories;
    }
    else{
      return null;
    }
  }

}
?>