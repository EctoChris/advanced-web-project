<?php
 class ShoppingCart extends Database{
    public $productIds = array();
    public $productQuantities = array();
    public $productCount = 0;
    public $totalProductsInCart = 0;
    public $shoppingCartProducts = array();
    public $productsInCart = array();
    public $extraProductInfo;
    
   public function __construct()
   {
        parent::__construct();
   }

   public function returnTotalProducts()
   {
        $this -> totalProductsInCart = 0;
        $query = 'SELECT * FROM shoppingcart WHERE userid = ?';
       
        //send the query to the database using the database class connection variable
        $statement = $this -> connection -> prepare($query);
    
        $statement -> bind_param('s',$_SESSION['id']);

        //execute query
        $statement -> execute();

        //get query results
        $result = $statement -> get_result();
        
        $totalP = 0;
         while( $row = $result -> fetch_assoc() )
         {
             $totalP += $row['quantity'];
         }
        return $totalP;
   }
   
    
    public function addToShoppingCart($productId, $productQuantity, $userId)
    {
        $this -> productCount = count([$productIds]);
        $query = 'INSERT INTO  shoppingcart ( productid, quantity, userid ) VALUES (?, ?, ?)';
        
        //send the query to the database using the database class connection variable
        $statement = $this -> connection -> prepare($query);
    
        //Bind paramaters to query
        $statement -> bind_param('iii',$productId, $productQuantity, $userId);

        //execute query
        $statement -> execute();

        return;
    }
    
    public function getProductsInCart($userId)
    {
       $query = "SELECT productid, SUM(quantity) AS quantity, userid 
                 FROM `shoppingcart` WHERE (userid = ?)
                 GROUP BY productid";
       $statement = $this -> connection -> prepare($query);
       
       $statement -> bind_param('i', $userId);
       
       $statement -> execute();
       
       //get query results
       $result = $statement -> get_result();
       if( $result -> num_rows > 0 )
       {
           while( $row = $result -> fetch_assoc() )
           {
            //add each row to products array
            array_push( $this -> productsInCart, $row );
           }
       }
    }
    
    public function getProductNameAndPrice($productId)
    {
      $this -> extraProductInfo = null;
      
      $query = "SELECT name, price, imagename
                FROM products
                WHERE (id = ?);";
      $statement = $this -> connection -> prepare($query);
      
      $statement -> bind_param('i', $productId);
      
      $statement -> execute();
      
       //get query results
       $result = $statement -> get_result();
       if( $result -> num_rows > 0 )
       {
           while( $row = $result -> fetch_assoc() )
           {
            $this -> extraProductInfo = $row;
           }
       }
    }
    
    public function deleteProductFromCart ($productId, $userId)
    {
     $query = "DELETE FROM `shoppingcart` WHERE (productid = ? AND userid = ?);";
     
     $statement = $this -> connection -> prepare($query);
     
     $statement -> bind_param('ii', $productId, $userId);
     
     $statement -> execute();
     
     return;
    }
    
    public function emptyShoppingCart ($userId)
    {
     $query = "DELETE FROM `shoppingcart` WHERE (userid = ?);";
     
     $statement = $this -> connection -> prepare($query);
     
     $statement -> bind_param('i', $userId);
     
     $statement -> execute();
     
     return;
    }
    
    
}


?>