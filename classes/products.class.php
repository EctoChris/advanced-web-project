<?php
//modified product class to accept pagination
class Products extends Database {
  //result array for products
  public $products = array();
  public $products_total = 15;
  public $current_page_total;
  private $searchWord = "";
  private $page_size = 6;
  private $current_page = 1;
  private $offset = 0;
  private $total_pages;
  private $selected_category = 0;
  
  public function __construct(){
    parent::__construct();
    //check get variables in the request
    
    if( isset( $_GET["page"] ) ){
      $this -> current_page = $_GET["page"];
    }
    if( isset( $_GET["page_size"] ) ){
      $this -> page_size = $_GET["page_size"];
    }
    
    $this -> offset = ($this -> current_page - 1 ) * $this -> page_size;
    
    //Check if Category Selected
    if( isset( $_GET["categorySelection"]))
    {
      $category_var = $_GET["categorySelection"];
      $this -> selected_category = $category_var;
    }
    
    //Check if word was searched for
    if (isset ( $_GET["searchTerm"]))
    {
      $search_var = $_GET["searchTerm"];
      $this -> searchWord = $search_var;
    }
  }
  
  public function getProducts($pagination = false){

    if($this -> selected_category !== 0 && (strlen($this -> searchWord) == 0))
    {
      //Check if category is selected
      $query = "SELECT * FROM products, categories 
                WHERE products.id = categories.productid AND categories.categoryid = ?";
    }
    else if (strlen($this -> searchWord) > 0)
    {
      $query = "SELECT * from products
                WHERE products.name LIKE ?
                GROUP BY products.id";
                // OR products.description LIKE ?";
    }
    else 
    {
      $query = 'SELECT * FROM products';
    }
    
    //get the total number of products
    $this -> products_total = $this -> getProductsTotal( $query );
  
    //if pagination is true append limit and offset to query
    if($pagination == true){
      $query = $query . ' ' . 'LIMIT ? OFFSET ?';
    }
    //send the query to the database using the database class connection variable
    $statement = $this -> connection -> prepare($query);
    
    //if there is category selected, pass the parameters
    
    if ($this -> selected_category !== 0 && (strlen($this-> searchWord) == 0))
    {
      $statement -> bind_param('i', $this -> selected_category);
    }
    else if (strlen($this -> searchWord) > 0)
    {
      $test = $this -> searchWord;
      $likeTerm = '%' . $test . '%';
      $statement -> bind_param('s', $likeTerm);
    }
    //if pagination is true bind the parameters
    else {
      $statement -> bind_param('ii', $this -> page_size, $this -> offset );
    }
    
    //execute query
    $statement -> execute();
    //get query results
    $result = $statement -> get_result();
    if( $result -> num_rows > 0 )
    {
      while( $row = $result -> fetch_assoc() )
      {
        //add each row to products array
        array_push( $this -> products, $row );
      }
      
      $this -> current_page_total = $result -> num_rows;
      
      //for future class AJAX
      if( $json == false )
      {
        return $this -> products;
      }
      else{
        return json_encode( $this -> products );
      }
    }
    else{
      $this -> current_page_total = 0;
      return false;
    }
    $statement -> close();
  }
  
  public function getBestSellers()
  {
    $query = "SELECT * FROM  `products` 
              ORDER BY unitssold DESC 
              LIMIT 3";
              
    //send the query to the database using the database class connection variable
    $statement = $this -> connection -> prepare($query);
    
    //execute query
    $statement -> execute();

    //get query results
    $result = $statement -> get_result();
     if( $result -> num_rows > 0 )
     {
       while( $row = $result -> fetch_assoc() )
       {
        //add each row to products array
        array_push( $this -> products, $row );
       }
     }
  }
  
  
  private function getProductsTotal( $query ) 
  {
    //execute the query and return the total
    $statement = $this -> connection -> prepare( $query );
    $statement -> execute();
    $result = $statement -> get_result();
    $rows = $result -> num_rows;
    return $rows;
  }
  
}
?>